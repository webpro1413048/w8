import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Role } from 'src/roles/entities/role.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    for (let i = 0; i < createUserDto.roles.length; i++) {
      const id = createUserDto.roles[i].id;

      if (!this.rolesRepository.findOneBy({ id })) {
        return this.rolesRepository.findOneByOrFail({ id });
      }
    }
    return this.usersRepository.save(createUserDto);
  }

  findAll() {
    return this.usersRepository.find({ relations: { roles: true } });
  }

  async findOne(id: number) {
    this.usersRepository.findOneByOrFail({ id });
    return await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    await this.usersRepository.findOneByOrFail({ id });
    await this.usersRepository.update(id, updateUserDto);
    const updatedUser = this.usersRepository.findOneBy({ id });
    return updatedUser;
  }

  async remove(id: number) {
    const user = await this.usersRepository.findOneByOrFail({ id });
    await this.usersRepository.remove(user);
    return user;
  }
}
