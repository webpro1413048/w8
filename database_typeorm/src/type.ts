import { AppDataSource } from "./data-source";
import { Type } from "./entity/Type";

AppDataSource.initialize()
  .then(async () => {
    const typeRepository = AppDataSource.getRepository(Type);

    await typeRepository.clear();

    var type = new Type();
    type.id = 1;
    type.name = "Drink";
    await typeRepository.save(type);

    type = new Type();
    type.id = 2;
    type.name = "Coffee";
    await typeRepository.save(type);

    type = new Type();
    type.id = 3;
    type.name = "Food";
    await typeRepository.save(type);

    var findUser = await typeRepository.find();
    console.log(findUser);
  })
  .catch((error) => console.log(error));
