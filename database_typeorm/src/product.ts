import { AppDataSource } from "./data-source";
import { Product } from "./entity/Product";
import { Type } from "./entity/Type";

AppDataSource.initialize()
  .then(async () => {
    const productRepository = AppDataSource.getRepository(Product);
    const typeRepository = AppDataSource.getRepository(Type);

    await productRepository.clear();
    const drinkType = await typeRepository.findOneBy({ id: 1 });
    const coffeeType = await typeRepository.findOneBy({ id: 2 });
    const foodType = await typeRepository.findOneBy({ id: 3 });

    var product = new Product();
    product.id = 1;
    product.name = "CocaCola";
    product.price = 100;
    product.type = drinkType;
    await productRepository.save(product);

    product = new Product();
    product.id = 2;
    product.name = "Capuchino";
    product.price = 200;
    product.type = coffeeType;
    await productRepository.save(product);

    product = new Product();
    product.id = 3;
    product.name = "Arabica";
    product.price = 300;
    product.type = coffeeType;
    await productRepository.save(product);

    product = new Product();
    product.id = 4;
    product.name = "Americano";
    product.price = 400;
    product.type = coffeeType;
    await productRepository.save(product);

    product = new Product();
    product.id = 5;
    product.name = "Donut";
    product.price = 500;
    product.type = foodType;
    await productRepository.save(product);

    var findProduct = await productRepository.find({
      relations: { type: true },
    });
    console.log(findProduct);
  })

  .catch((error) => console.log(error));
