import { Injectable } from '@nestjs/common';
import { CreateTypeDto } from './dto/create-type.dto';
import { UpdateTypeDto } from './dto/update-type.dto';
import { Type } from './entities/type.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class TypesService {
  constructor(
    @InjectRepository(Type) private typesRepository: Repository<Type>,
  ) {}
  create(createTypeDto: CreateTypeDto) {
    return this.typesRepository.save(createTypeDto);
  }

  findAll() {
    return this.typesRepository.find();
  }

  async findOne(id: number) {
    return this.typesRepository.findOneByOrFail({ id: id });
  }

  async update(id: number, updateTypeDto: UpdateTypeDto) {
    await this.typesRepository.findOneByOrFail({ id });
    this.typesRepository.update(id, updateTypeDto);
    const updateType = this.typesRepository.findOneBy({ id });
    return updateType;
  }

  async remove(id: number) {
    const removeType = await this.typesRepository.findOneByOrFail({ id });
    await this.typesRepository.remove(removeType);
    return removeType;
  }
}
